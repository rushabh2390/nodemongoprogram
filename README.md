Perform  mongo workshop in node js from below link <br/>
https://github.com/evanlucas/learnyoumongo<br/>
prog1.js: Create and verify mongodb<br/>
prog2.js: Connect to mongodb<br/>
prog3.js: Get data using find in mongo db<br/>
prog4.js: Get data using find and project(get only required fields) in mongo db<br/>
prog5.js: Insert data into mongo db collection using insert function<br/>
prog6.js: Update data into mongo db collection using update function<br/>
prog7.js: Remove data into mongo db collection using remove function<br/>
prog8.js: Count data into mongo db collection using count funtion<br/>
prog9.js: aggrgate data into mongo db collection using aggrgate function<br/>